﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : MonoBehaviour
{
    public MenuController menuController;
    private int previousPage;

    public void GoLeft()
    {
        previousPage = menuController.actualPage;

        menuController.actualPage--;
        if (menuController.actualPage == 0)
            menuController.actualPage = menuController.maxPage;

        menuController.SwitchButtons(previousPage, menuController.actualPage);
    }

    public void GoRight()
    {
        previousPage = menuController.actualPage;

        menuController.actualPage++;
        if (menuController.actualPage == menuController.maxPage + 1)
            menuController.actualPage = 1;

        menuController.SwitchButtons(previousPage, menuController.actualPage);
    }
}
