﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BusLineController : MonoBehaviour
{
    public void SwitchVisibility() {
        this.gameObject.SetActive(!gameObject.activeSelf);
    }
}
