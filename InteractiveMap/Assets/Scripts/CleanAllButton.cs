﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CleanAllButton : MonoBehaviour
{
    private GameObject[] busLines;

    void Start()
    {
        busLines = GameObject.FindGameObjectsWithTag("BusLine");
    }

    public void CleanAllBusLines()
    {
        foreach (GameObject busLine in busLines)
        {
            busLine.SetActive(false);
        }
    }
}
