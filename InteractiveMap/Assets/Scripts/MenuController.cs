﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    public int actualPage = 1;
    public int maxPage = 2;

    private GameObject[] busLineButtons;

    void Start()
    {
        busLineButtons = GameObject.FindGameObjectsWithTag("BusLineButton");

        foreach(GameObject busLineButton in busLineButtons)
        {
            if (busLineButton.GetComponent<ButtonController>().buttonPage != 1)
            {
                busLineButton.GetComponent<MeshRenderer>().enabled = false;
                busLineButton.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
                busLineButton.GetComponent<CapsuleCollider>().enabled = false;
            }
        }
    }

    public void SwitchButtons(int previousPage, int actualPage)
    {
        foreach (GameObject busLineButton in busLineButtons)
        {
            if (busLineButton.GetComponent<ButtonController>().buttonPage == previousPage)
            {
                busLineButton.GetComponent<MeshRenderer>().enabled = false;
                busLineButton.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
                busLineButton.GetComponent<CapsuleCollider>().enabled = false;
            }
            else if (busLineButton.GetComponent<ButtonController>().buttonPage == actualPage)
            {
                busLineButton.GetComponent<MeshRenderer>().enabled = true;
                busLineButton.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                busLineButton.GetComponent<CapsuleCollider>().enabled = true;
            }
        }
    }
}
