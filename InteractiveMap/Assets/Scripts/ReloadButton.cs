﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadButton : MonoBehaviour
{
    private GameObject[] busLines;

    void Start()
    {
        busLines = GameObject.FindGameObjectsWithTag("BusLine");
    }

    public void ActivateAllBusLines()
    {
        foreach(GameObject busLine in busLines)
        {
            busLine.SetActive(true);
        }
    }
}
